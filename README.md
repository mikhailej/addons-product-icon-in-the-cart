# Module: product icon in the shopping cart #

Changes the size of the product icon in the cart.

### Installing the module ###

To install this module, copy all the files to / cs-cart / with the folder structure preserved. 